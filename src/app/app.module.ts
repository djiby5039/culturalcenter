import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { FormationComponent } from './formation/formation.component';
import { AboutComponent } from './about/about.component';
import { EvenementComponent } from './evenement/evenement.component';
import { InternationalComponent } from './international/international.component';
    


const appRoutes: Routes =[
    {path: "Home", component: HomeComponent},
    {path: "Formation", component: FormationComponent},
    {path: "Evenement", component: EvenementComponent},
    {path: "International", component: InternationalComponent},
    {path: "About", component: AboutComponent}
  
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FormationComponent,
    AboutComponent,
    EvenementComponent,
    InternationalComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
